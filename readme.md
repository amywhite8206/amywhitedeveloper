#amywhitedeveloper: A03

A resume site using Node, Express, Bootstrap, EJS

##How to use

    - Fork into your own cloud account.
    - Clone the site to your machine
    - Open a command wino in the new folder
    - Run npm install to install dependencies listed in package.json
    - Run node app.js to start the server. (Hit CTRL-C to stop).

...
> npm install
> npm app.js
...

Point your browser to 'http://localhost.8081'